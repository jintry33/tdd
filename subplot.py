import matplotlib.pyplot as plt
#main test 110822, 110822-1
x = [0,1,2,3,4,5,6,7,8]

y_dict = dict (
y_0 = [0,0,0,0,1,0,0,0,0],
y_1 = [1,0,0,0,0,0,0,0,0],
y_2 = [0,0,0,0,0,0,0,0,1],
y_3 = [1,0,0,0,0,0,0,0,1],
y_4 = [1,0,0,0,0,0,0,0,1],
y_5 = [1,0,0,0,0,0,0,0,1],
y_6 = [1,0,0,0,0,0,0,0,1],
)


c_dict = dict (
c_0 = ["c","c","c","c","orange","c","c","c","c"],
c_1 = ["b","c","c","c","c","c","c","c","c"],
c_2 = ["c","c","c","c","c","c","c","c","green"],
c_3 = ["m","c","c","c","c","c","c","c","c"],
c_4 = ["y","c","c","c","c","c","c","c","c"],
c_5 = ["r","c","c","c","c","c","c","c","c"],
c_6 = ["black","c","c","c","c","c","c","c","c"],
)

w = [0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5]
k = 12 # 3 for LTE, 7 for u0, 12 for u1

for i in range(7):
    y = y_dict["y_" + str(i)]
    c = c_dict["c_" + str(i)]
    if i < 4:
        w[0] = 0.4
    else:
        w[0] += 0.4
    # reset to 5m bandwidth
    w[8] = 0.4
    
    # bw start point setup
    num = 10
    
    # #IBW 100MHz limitation apply
    # if i > 2:
    #     k -= 1
        
    for j in range(k):
        if k <= 4:
            text = str(num) +"m(LTE)"
        if 4 < k <=8:
            text = str(num) +"m(nr u0)"
        if 8 < k <=13:
            text = str(num) +"m(nr u1)"
        if num < 28:
            num += 5
        else: 
            num += 10
        
        plt.subplot2grid((7,k), (i,j))
        plt.bar(x, y, width = w, color = c)
        plt.text(5.5, 0.5, str(text), fontsize=12, ha='center')
        if y == [0,0,0,0,1,0,0,0,0]:
            w[4] += 0.3
        if y == [1,0,0,0,0,0,0,0,0]:
            w[0] += 0.4
        if y == [0,0,0,0,0,0,0,0,1]:
            w[8] += 0.4
        if y == [1,0,0,0,0,0,0,0,1]:
            w[8] += 0.4

        if i ==0 and j==0:
            plt.ylabel("center freq")
        if i ==1 and j==0:
            plt.ylabel("left freq")
        if i ==2 and j==0:
            plt.ylabel("right freq")
        if i ==3 and j==0:
            plt.ylabel("multi carrier_0")
        if i ==4 and j==0:
            plt.ylabel("multi carrier_1")
        if i ==5 and j==0:
            plt.ylabel("multi carrier_2")
        if i ==6 and j==0:
            plt.ylabel("multi carrier_3")
        plt.xticks([], [])
        plt.yticks([], [])
        
        
plt.show()