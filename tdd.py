import numpy as np
import matplotlib.pyplot as plt
import string
from PyQt5.QtCore import reset
import sys
from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QAxContainer import *
from PyQt5.QtGui import *

form_class = uic.loadUiType("tdd.ui")[0]

class WindowClass(QMainWindow, form_class):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        urlLink="<a href=\"http://www.sharetechnote.com\">'Click this link to go to TDD config'</a>" 
        # label_10=QtGui.QLabel(self)
        self.label_10.setText(urlLink)
        self.label_10.setOpenExternalLinks(True)
        print (self.LTE_NR.currentText())
        self.Run.clicked.connect(self.lte_tdd_timing)
        self.Run_2.clicked.connect(self.nr_tdd_timing)
        
    def lte_tdd_timing(self):

        #'LTE_TDDConfig'
        LTE_TDDConfig_dict = dict (
        LTE_TDDConfig_0 = ["d", "s", "u", "u" ,"u" ,"d" ,"s" ,"u" ,"u" ,"u"],
        LTE_TDDConfig_1 = ["d", "s", "u", "u" ,"d" ,"d" ,"s" ,"u" ,"u" ,"d"],
        LTE_TDDConfig_2 = ["d", "s", "u", "d" ,"d" ,"d" ,"s" ,"u" ,"d" ,"d"],
        LTE_TDDConfig_3 = ["d", "s", "u", "u" ,"u" ,"d" ,"d" ,"d" ,"d" ,"d"],
        LTE_TDDConfig_4 = ["d", "s", "u", "u" ,"d" ,"d" ,"d" ,"d" ,"d" ,"d"],
        LTE_TDDConfig_5 = ["d", "s", "u", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"d"],
        LTE_TDDConfig_6 = ["d", "s", "u", "u" ,"u" ,"d" ,"s" ,"u" ,"u" ,"u"]
        )

        # graph define for LTE_TDDConfig
        LTE_TDDConfig = []
        x = [0,1,2,3,4,5,6,7,8,9] 
        c = [1,1,1,1,1,1,1,1,1,1]
        y = [1,1,1,1,1,1,1,1,1,1]
        
        # select TDD config by GUI input
        LTE_TDDConfig = LTE_TDDConfig_dict['LTE_TDDConfig_'+str(self.LTE_u_d.currentText())]

        # graph color define for LTE_TDDConfig
        for i in range(len(LTE_TDDConfig)):
            if LTE_TDDConfig[i] == "d":
                y[i] = 1
                c[i] = "blue"
            elif LTE_TDDConfig[i] == "u":
                y[i] = 0.1
                c[i] = "orange"
            elif LTE_TDDConfig[i] == "s":
                y[i] = 1
                c[i] = "gray"
                
        # LTE_SSC       
        LTE_SSC_dict = dict (      
        LTE_SSC_0 = ["d", "d", "d", "g" ,"g" ,"g" ,"g" ,"g" ,"g" ,"g", "g" ,"g" ,"g" ,"u"],
        LTE_SSC_1 = ["d", "d", "d", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"g", "g" ,"g" ,"g" ,"u"],
        LTE_SSC_2 = ["d", "d", "d", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"d", "g" ,"g" ,"g" ,"u"],
        LTE_SSC_3 = ["d", "d", "d", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"d", "d" ,"g" ,"g" ,"u"],
        LTE_SSC_4 = ["d", "d", "d", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"d", "d" ,"d" ,"g" ,"u"],
        LTE_SSC_5 = ["d", "d", "d", "g" ,"g" ,"g" ,"g" ,"g" ,"g" ,"g", "g" ,"g" ,"u" ,"u"],
        LTE_SSC_6 = ["d", "d", "d", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"g", "g" ,"g" ,"u" ,"u"],
        LTE_SSC_7 = ["d", "d", "d", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"d", "g" ,"g" ,"u" ,"u"],
        LTE_SSC_8 = ["d", "d", "d", "d" ,"d" ,"d" ,"d" ,"d" ,"d" ,"d", "d" ,"g" ,"u" ,"u"],
        LTE_SSC_9 = ["d", "d", "d", "d" ,"d" ,"d" ,"g" ,"g" ,"g" ,"g", "g" ,"g" ,"u" ,"u"],
        LTE_SSC_10 = ["d", "d", "d", "d" ,"d" ,"d" ,"g" ,"g" ,"u" ,"u", "u" ,"u" ,"u" ,"u"]
        )

        LTE_SSC = []
        x2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13] 
        y2 = [1,1,1,1,1,1,0,0,0,0,1,1,1,1]
        c2 = [1,1,1,1,1,1,0,0,0,0,1,1,1,1]
        
        LTE_SSC = LTE_SSC_dict['LTE_SSC_'+str(self.LTE_ssf.currentText())]


        x5 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13] 
        c5 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        y5 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        w5 = [0.4,1.4,0.5,1,0.5,1,0.5,1,0.5,1,0.5,1,0.5,1]

        for i in range(len(LTE_SSC)):
            if LTE_SSC[i] == "d":
                y2[i] = 1
                c2[i] = "blue"
            elif LTE_SSC[i] == "u":
                y2[i] = 0.1
                c2[i] = "orange"
            elif LTE_SSC[i] == "g":
                y2[i] = 1
                c2[i] = "gray"
                
        for i in range(len(LTE_SSC)):
            if LTE_SSC[i] == "d":
                y5[i] = 1
                c5[i] = "blue"
            elif LTE_SSC[i] == "u":
                y5[i] = 0.1
                c5[i] = "orange"
            elif LTE_SSC[i] == "g":
                y5[i] = 1
                c5[i] = "gray"
                
                
        plt.figure(figsize=(10, 10))
        plt.subplots_adjust(top = 0.99, bottom=0.1, hspace=0.4, wspace=1.0)
        plt.subplot(3,1,1)
        plt.ylabel("TDD Config")
        plt.xlabel("Blue => DL, Orange => UL, Gray => S")
        plt.xticks(np.arange(min(x), max(x)+1, 1.0))
        plt.yticks(np.arange(min(x), max(x)+1, 1.0))
        plt.bar(x, y, color =c, width=0.98, label = "LTE_TDDConfig = "+ str(self.LTE_u_d.currentText()))
        plt.legend(loc="best")

        plt.subplot(3,1,2)
        plt.ylabel("Subframe Config")
        plt.xlabel("Blue => dwPTS, Orange => upPTS, Gray => GP")
        plt.xticks(np.arange(min(x2), max(x2)+1, 1.0))
        plt.yticks(np.arange(min(x2), max(x2)+1, 1.0))
        plt.bar(x2, y2, color =c2, width=0.98, label = "LTE_SSC =" + str(self.LTE_ssf.currentText()))
        plt.legend(loc="best")
        plt.show()
        
        plt.subplot(3,1,3)
        plt.xticks(np.arange(min(x5), max(x5)+1, 1.0))
        plt.yticks(np.arange(min(x5), max(x5)+1, 1.0))
        plt.xlabel(" Blue => DL, Orange => UL, Gray => F")
        plt.ylabel("CP Config")
        plt.bar(x5, y5, color =c5, width=w5)
        plt.legend(loc="best")
        plt.show()
        
    def nr_tdd_timing(self):    
    #'nr_TDDConfig for u0'
        mu = int(self.NR_u_value.currentText())
        num_Slots_per_frame = 10 * (2**(mu))
        
        dl_UL_Pe = float(self.dl_UL_Pe.currentText())
        
        print (type(num_Slots_per_frame))
        num_symbol_per_Slot = 14
        print ("num_symbol_per_Slot", num_symbol_per_Slot)
        
        #Print GUI value
        print (self.NR_u_value.currentText())
        print (self.dl_UL_Pe.currentText())
        print (self.dl_Slots.currentText())
        print (self.dl_Symbols.currentText())
        print (self.ul_Slots.currentText())
        print (self.ul_Symbols.currentText())
        
        #'NR_TDDConfig'
        # NR_TDDConfig = list (range(0, num_Slots_per_frame))
        NR_TDDConfig = ["s"] * num_Slots_per_frame
        print ("NR_TDDConfig is ",NR_TDDConfig)
    

        
        for i in range (int(self.dl_Slots.currentText())):
            print(i)
            NR_TDDConfig[i] ="d"
            if dl_UL_Pe == 5:
                NR_TDDConfig[i+(5* 2** (mu))] ="d"
            elif dl_UL_Pe == 2.5: # need fix
                NR_TDDConfig[i+5] ="d"
                NR_TDDConfig[i+10] ="d"
                NR_TDDConfig[i+15] ="d"
                                    
        print ("NR_TDDConfig is ",NR_TDDConfig)
        
        slot_end = 10 * (int(self.NR_u_value.currentText()) + 1) - int(self.ul_Slots.currentText()) -1
        print ("ul_Slots.currentText value in reverse is : ", slot_end)
        
        for y in range (num_Slots_per_frame -1, slot_end, -1):
            NR_TDDConfig[y] ="u"
            if dl_UL_Pe == 5:
                NR_TDDConfig[y-(5* 2** (mu))] ="u"
            if dl_UL_Pe == 2.5: # need fix
                NR_TDDConfig[y-15] ="u"
                NR_TDDConfig[y-10] ="u"
                NR_TDDConfig[y-5] ="u"
        print ("NR_TDDConfig is ",NR_TDDConfig)
    
    
        # graph define for NR_TDDConfig
        
        if mu == 0:        
            x3 = [0,1,2,3,4,5,6,7,8,9] 
            c3 = [1,1,1,1,1,1,1,1,1,1]
            y3 = [1,1,1,1,1,1,1,1,1,1]
        
        elif mu == 1:
            x3 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19] 
            c3 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            y3 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            
        # graph color define for NR_TDDConfig
        for i in range(len(NR_TDDConfig)):
            if NR_TDDConfig[i] == "d":
                y3[i] = 1
                c3[i] = "blue"
            elif NR_TDDConfig[i] == "u":
                y3[i] = 0.1
                c3[i] = "orange"
            elif NR_TDDConfig[i] == "s":
                y3[i] = 1
                c3[i] = "gray"
                
                
        # NR subframe define
        num_symbol_per_Slot =["f"] * num_symbol_per_Slot
        print ("num_symbol_per_Slot", num_symbol_per_Slot)
        
        for i in range (int(self.dl_Symbols.currentText())):
            num_symbol_per_Slot[i] ="d"
        print ("num_symbol_per_Slot is ",num_symbol_per_Slot)
        
        symbol_end = 14 - int(self.ul_Symbols.currentText()) -1
        print ("ul_Slots.currentText value in reverse is : ", symbol_end)
        
        for ul in range (13, symbol_end, -1):
            num_symbol_per_Slot[ul] ="u"
            
        print ("num_symbol_per_Slot is ",num_symbol_per_Slot)
        
        print (self.ul_Symbols.currentText())
        
        x4 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13] 
        c4 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        y4 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        
        
        for i in range(len(num_symbol_per_Slot)):
            if num_symbol_per_Slot[i] == "d":
                y4[i] = 1
                c4[i] = "blue"
            elif num_symbol_per_Slot[i] == "u":
                y4[i] = 0.1
                c4[i] = "orange"
            elif num_symbol_per_Slot[i] == "f":
                y4[i] = 1
                c4[i] = "gray"
                
        # NR CP define
        w4 = [0.4,1.4,0.5,1,0.5,1,0.5,1,0.5,1,0.5,1,0.5,1]  
                  
        plt.figure(figsize=(10, 5))
        plt.subplots_adjust(top = 0.99, bottom=0.1, hspace=0.4, wspace=1.0)
        plt.subplot(2,1,1)
        plt.xticks(np.arange(min(x3), max(x3)+1, 1.0))
        plt.yticks(np.arange(min(x3), max(x3)+1, 1.0))
        plt.xlabel("Blue => DL, Orange => UL, Gray => S")
        plt.ylabel("TDD Config")
        plt.bar(x3, y3, color =c3, width=0.95, label = "NR_TDDConfig = mu"+ str(self.NR_u_value.currentText())+"_"+ str(self.dl_UL_Pe.currentText())+"_"+ \
            str(self.dl_Slots.currentText())+ str(self.dl_Symbols.currentText())+ str(self.ul_Slots.currentText())+ str(self.ul_Symbols.currentText()))
        plt.legend(loc="best")
        
        plt.subplot(2,1,2)
        plt.xticks(np.arange(min(x4), max(x4)+1, 1.0))
        plt.yticks(np.arange(min(x4), max(x4)+1, 1.0))
        plt.xlabel("Blue => DL, Orange => UL, Gray => F")
        plt.ylabel("Subframe Config")
        plt.bar(x4, y4, color =c4, width=0.95)
        plt.legend(loc="best")
        plt.show()
                    
        # plt.subplot(3,1,3)
        # plt.xticks(np.arange(min(x4), max(x4)+1, 1.0))
        # plt.yticks(np.arange(min(x4), max(x4)+1, 1.0))
        # plt.xlabel(" Blue => DL, Orange => UL, Gray => F")
        # plt.ylabel("CP Config")
        # plt.bar(x4, y4, color =c4, width=w4)
        # plt.legend(loc="best")
        # plt.show()
        
        

if __name__ == "__main__" :
    #QApplication : 
    app = QApplication(sys.argv)

    #WindowClass
    myWindow = WindowClass()

    #show screen code
    myWindow.show()

    #event loop enter
    app.exec_()
