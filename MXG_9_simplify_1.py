#Download NI-VISA
#pip install keysight
#pip install visa
#pip install -U pyvisa
#import keysight.command_expert_py3


import sys
from PyQt5.QtWidgets import *
from PyQt5 import uic
import pyvisa as visa
import time
from telnetlib import Telnet
WAIT_TIME = 1 # sec
from functools import partial

#UI파일 연결
#단, UI파일은 Python 코드 파일과 같은 디렉토리에 위치해야한다.
form_class = uic.loadUiType("ARB.ui")[0]

#화면을 띄우는데 사용되는 Class 선언
class WindowClass(QMainWindow, form_class) :
    def __init__(self) :
        super().__init__()
        self.setupUi(self)
        #버튼에 기능을 연결하는 코드
        self.pushButton.clicked.connect(self.arbon_text)
        self.pushButton.clicked.connect(self.arboff)
        self.pushButton_2.clicked.connect(self.arboff_text)
        self.pushButton_2.clicked.connect(self.arbon)
        self.pushButton_3.clicked.connect(partial(self.switch_freq, 'f1'))  # freq = f1
        self.pushButton_4.clicked.connect(partial(self.switch_freq, 'f2'))  # freq = f2
        self.pushButton_5.clicked.connect(partial(self.switch_level, 'l1'))  # level = l1
        self.pushButton_6.clicked.connect(partial(self.switch_level, 'l2'))  # level = l2
        self.radioButton.pressed.connect(partial(self.switch_arb, 'lte05'))  # arb = lte05
        self.radioButton_2.pressed.connect(partial(self.switch_arb, 'lte10'))  # arb = lte10
        self.radioButton_3.pressed.connect(partial(self.switch_arb, 'lte15'))  # arb = lte15
        self.radioButton_4.pressed.connect(partial(self.switch_arb, 'lte20'))  # arb = lte20
        self.radioButton_5.pressed.connect(partial(self.switch_sig, 'lte05'))  # sig = lte05
        self.radioButton_6.pressed.connect(partial(self.switch_sig, 'lte10'))  # sig = lte10
        self.radioButton_7.pressed.connect(partial(self.switch_sig, 'lte15'))  # sig = lte15
        self.radioButton_8.pressed.connect(partial(self.switch_sig, 'lte20'))  # sig = lte20
        #self.lineEdit.setText(self.PlainText)
        #self.lineEdit_2.setText(self.PlainText_2)
        self.ip = self.lineEdit_3.text()

    def telnet_write(self, message, port_no, timeout_count):
        self.ip = self.lineEdit_3.text()

        try:
            with Telnet(self.ip, port_no, timeout=timeout_count) as tn: # hostip = self.ip, port = 5025, timeout = 90
                tn.write(bytes(message, encoding='utf8'))  #str -> byte
                time.sleep(WAIT_TIME)
                
        except Exception as err:
            print(err)

        except KeyboardInterrupt:
            print('terminated by the user')
            exit(0)

        finally:
            print('Done.')

    def arboff(self):
        message = f':SOURce:RADio:ARB:STATe OFF\n'
        port_no = 5025
        timeout_count = 90
        
        #self.telnet_write(message, port_no, timeout_count)
        self.telnet_write()
    def arbon(self):
        message = f':SOURce:RADio:ARB:STATe ON\n'
        port_no = 5025
        timeout_count = 90
        
        self.telnet_write(message, port_no, timeout_count)

    def arbon_text(self):
        self.label.setText("ARB OFF")

    def arboff_text(self):
        self.label.setText("ARB ON")

    def switch_freq(self, key):
        if key == 'f1':
            text = self.lineEdit.text()
            text_freq = text+'MHZ'
        elif key == 'f2':
            text = self.lineEdit_2.text()
            text_freq = text+'MHZ'

        self.label_2.setText(text)

        message = f':SOURce:FREQuency '+text_freq+'\n'
        port_no = 5025
        timeout_count = 90
        self.telnet_write(message, port_no, timeout_count)
        
        print (key)
        print (message)

    def switch_level(self, key):
        if key == 'l1':
            text = self.lineEdit_4.text()
        elif key == 'l2':
            text = self.lineEdit_5.text()

        self.label_6.setText(text)

        message = f':SOURce:POWer:LEVel '+text+'\n'
        port_no = 5025
        timeout_count = 90

        self.telnet_write(message, port_no, timeout_count)

        print (key)
        print (message)

    def switch_arb(self, key):
        if key == 'lte05':
            self.label_3.setText("7.68Msps")
            message = f':SOURce:RADIO:ARB:SCLock:RATE 7680000\n'
        elif key == 'lte10':
            self.label_3.setText("15.36Msps")
            message = f':SOURce:RADIO:ARB:SCLock:RATE 15360000\n'
        elif key == 'lte15':
            self.label_3.setText("23.04Msps")
            message = f':SOURce:RADIO:ARB:SCLock:RATE 23040000\n'
        elif key == 'lte20':
            self.label_3.setText("30.72Msps")
            message = f':SOURce:RADIO:ARB:SCLock:RATE 30720000\n'

        port_no = 5025
        timeout_count = 90

        self.telnet_write(message, port_no, timeout_count)

        print (key)
        print (message)
    
    def switch_sig(self, key):
        if key == 'lte05':
            text = self.lineEdit_6.text()
        elif key == 'lte10':
            text = self.lineEdit_7.text()
        elif key == 'lte15':
            text = self.lineEdit_8.text()
        elif key == 'lte20':
            text = self.lineEdit_9.text()

        self.label_4.setText(text)

        message = f':SOURce:RADio:ARB:WAVeform "WFM1:'+text+'"\n'
        port_no = 5025
        timeout_count = 90

        self.telnet_write(message, port_no, timeout_count)

        print (key)
        print (message)
    
if __name__ == "__main__" :
    #QApplication : 프로그램을 실행시켜주는 클래스
    app = QApplication(sys.argv) 

    #WindowClass의 인스턴스 생성
    myWindow = WindowClass() 

    #프로그램 화면을 보여주는 코드
    myWindow.show()

    #프로그램을 이벤트루프로 진입시키는(프로그램을 작동시키는) 코드
    app.exec_()
